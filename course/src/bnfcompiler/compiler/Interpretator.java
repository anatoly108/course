package bnfcompiler.compiler;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Interpretator {
    // Сюда будем добавлять сообщения (как консоль)
    private ArrayList<String> messages = new ArrayList<String>();
    // Все слова
    private ArrayList<String> words = new ArrayList<String>();

    // Переменные для мат. выражения
    // Нет ли ошибки в выражении
    private boolean isExpressionOk = true;
    // �?ндекс текущего символа в выражении
    private int expressionSymbol = 0;
    // Выражение с ошибкой, если таковое есть
    private String badExpression = "";
    // Автомат Мура или Мили
    private boolean isMoore;

    private int depth = 0;

    // Начальный и конечный индексы "плохого" математического выражения
    private int badExpressionBeginIndex = 0;
    private int badExpressionEndIndex = 0;

    // �?ндекс начала выделения красным
    private int selectionStart = 0;
    // �?ндекс конца выделения красным
    private int selectionEnd = 0;

    private HashMap<String, Double> variables = new HashMap<String, Double>();

    private int i = 0; // Наш итератор (инкрементим функцией)

    // В режиме отладки выводятся дополнительные сообщения: пройденные слова; строки, полученные методами
    private boolean isDebug = false;

    public Interpretator() {

    }

    /**
     * Разбиение текста регулярным выражением с возможностью сохранения этого выражения
     *
     * @param text
     * @param regex
     * @param leaveRegex
     * @return ArrayList с разделённым текстом
     */
    public ArrayList<String> split(String text, String regex, boolean leaveRegex) {
        ArrayList<String> result = new ArrayList<String>();
        String[] splitRes = text.split(regex);
        for (int j = 0; j < splitRes.length; j++) {
            if (!splitRes[j].equals(""))
                result.add(splitRes[j]);
            if (leaveRegex) {
                // Добавляем regex везде, кроме конца
                if (j < splitRes.length - 1)
                    result.add(regex);
            }
        }
        return result;
    }

    /**
     * Этот метод нужен для определения слова, которое нужно выделить красным
     *
     * @return �?ндекс слова в words
     */
    public int getIterator() {
        return i;
    }

    /**
     * Функция компиляции
     * Манипулирует блоками текста между объектами элементов и выдаёт конечный результат компиляции true/false
     *
     * @param programText Текст программы
     * @return true/false Скомпилировалось/не скомпилировалось
     */
    public boolean compile(String programText) {

        words.clear();
        messages.clear();
        i = 0;
        isMoore = false;

        // Убираем управляющие символы
        programText = programText.replaceAll("\\r|\\n", " ");

        /**
         * Разбиваем на отдельные слова
         */

        ArrayList<String> splitedText = this.split(programText, ":", true);
        ArrayList<String> splitedTextTmp = new ArrayList<String>();
        for (String textToSplit : splitedText) {
            ArrayList<String> textToSplitSplited = this.split(textToSplit, " ", false);
            splitedTextTmp.addAll(textToSplitSplited);
        }
        splitedText = splitedTextTmp;
        words = splitedText;

        if (words.size() == 0) {
            return false;
        }

        if (!words.get(i).equals("программа")) {
            messages.add("Отсутствует 'программа'");
            return false;
        }

        if (!safeIncrement("После слова 'программа' задаются Звенья"))
            return false;

        // Должно быть хоть одно звено
        if (!link()) {
//            messages.add("После cлова 'программа' задаются Звенья");
            return false;
        }

        while (!words.get(i).equals("конец")) {

            if(!words.get(i).equals(";")){
                messages.add("Звенья разделяются точкой с запятой");
                return false;
            }

            if (!safeIncrement("Звено начинается с 'ввод'. Возможно, отсутствует слово 'конец'"))
                return false;

            if (!link()) {
                return false;
            }

        }

        if (!words.get(i).equals("конец")) {
            messages.add("Отсутствует 'конец'");
            return false;
        }

        return true;
    }

    private boolean link(){
        if (!words.get(i).equals("ввод")) {
            if(words.get(i).equals("конец")){
                messages.add("Не должно быть точки с запятой перед словом 'конец'");
                i--;
                return false;
            }

            messages.add("Звено начинается с 'ввод'");
            return false;
        }

        if (!safeIncrement("После 'ввод' идут операторы"))
            return false;

        while(!words.get(i).equals(";") && !words.get(i).equals("конец")){
            if(!operator())
                return false;
        }

        return true;
    }

    private boolean operator() {
        String variable = "";

        if (!mark()) {
            messages.add("Оператор начинается с метки");
            return false;
        }

        if (!safeIncrement("После метки должно быть двоеточие"))
            return false;

        if (!words.get(i).equals(":")) {
            messages.add("После метки должно быть двоеточие");
            return false;
        }

        if (!safeIncrement("После двоеточия должна быть переменная")) {
            return false;
        }

        if (!variable()) {
            return false;
        }

        variable = words.get(i);

        if (!safeIncrement("После переменной должен быть оператор '='")) {
            return false;
        }

        if (!words.get(i).equals("=")) {
            messages.add("После переменной должен быть оператор '='");
            return false;
        }

        if (!safeIncrement("После оператора '=' следует арифмитическое выражение (правая часть)")) {
            return false;
        }

        boolean allowMessages = true;

        String expression = "";
        boolean error = false;
        String errorMsg = "";
        badExpressionBeginIndex = i;
        while (i < words.size() && !words.get(i).equals("конец") && !words.get(i).equals(";")) {
            if (mark() && i < words.size() + 1) {
                // Второе слово ':'
                if (words.get(i + 1).equals(":")) {
                    break;
                }
            }

            if(i == words.size()-1){
                messages.add("Отсутствует 'конец");
                return false;
            }

            if (words.get(i + 1).equals(":")) {
                if (allowMessages) {
                    errorMsg = "Неверное имя для метки";
                }
                error = true;
                break;
            }

            char symbol = words.get(i).charAt(0);
            char lastSymbol = 0;
            if (!expression.equals(""))
                lastSymbol = expression.charAt(expression.length() - 1);

            boolean isSpaceAllowed = true;
            if (lastSymbol != 0)
                isSpaceAllowed = (symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/' || symbol == '|' || symbol == '&' || symbol == '(' || symbol == ')' || symbol == '[' || symbol == ']') || (lastSymbol == '+' || lastSymbol == '-' || lastSymbol == '*' || lastSymbol == '/' || lastSymbol == '|' || lastSymbol == '&' || lastSymbol == '(' || lastSymbol == ')' || lastSymbol == '[' || lastSymbol == ']');
            if (expression.equals("") || isSpaceAllowed) {
                expression += words.get(i);
            } else {
                if (allowMessages) {
                    errorMsg = "Пропущен знак операции";
                    if(words.get(i).equals("ввод")){
                        messages.add("Перед словом 'ввод' должна быть точка с запятой");
                        return false;
                    }
                }
                error = true;
                break;
            }
            i++;
        }
        if (error) {
            messages.add(errorMsg);
            return false;
        }

        badExpressionEndIndex = i - 1;

        depth = 0;
        double result = expression(expression);
        int res = (int) result;
        String strRes = Integer.toString(res, 8);
        double finalRes = Double.parseDouble(strRes);
        if (isExpressionOk) {
            variables.put(variable, finalRes);
            messages.add(variable + " = " + finalRes);
        } else {
            badExpression = expression;
            // Указываем на красным на expression
            i--;
            return false;
        }

        return true;
    }

    private boolean mark() {
        return words.get(i).matches("[-+]?[0-9]*\\.?[0-9]+");
    }

    private boolean variable() {
        return variable(true);
    }

    private boolean variable(boolean allowMessages) {
        String variable = words.get(i);
        return variable(allowMessages, variable);
    }

    private boolean variable(boolean allowMessages, String variable) {
        if (variable.length() != 4) {
            if (allowMessages)
                messages.add("Неверное имя переменной; переменная состоит из русской буквы и трёх цифр");
            return false;
        }

        int ascii = (int) variable.charAt(0) - 848;
        if (!(ascii >= 192 && ascii <= 255)) {//!(ascii >= 128 && ascii <= 159 || ascii >= 160 && ascii <= 175 || ascii >= 224 && ascii <= 241)) {
            if (allowMessages)
                messages.add("Название переменной должно начинаться с русской буквы");
            return false;
        }

        for (int i = 1; i < variable.length(); i++) {
            char letter = variable.charAt(i);
            int letterAscii = (int) letter;
            if (!(letter >= 48 && letter <= 57)) {
                if (allowMessages)
                    messages.add("В названии переменной после первой русской буквы могут быть только цифры");
                return false;
            }
        }
        if(variable.contains("8")||variable.contains("9")){
            if(allowMessages)
                messages.add("Только восьмеричная система счисления");
            return false;
        }

        return true;
    }

    /**
     * Подсчёт правой части
     *
     * @param expressionString строка, которую нужно подсчитать
     * @return число
     */
    private double expression(String expressionString) {

        double expression = 0;
        expressionSymbol = 0;
        isExpressionOk = true;
        // Есть ли минус
        boolean isMinus = false;

        if (expressionString.length() > 0) {
            if (expressionString.charAt(expressionSymbol) == '-') {
                expressionSymbol++;
                isMinus = true;
            }

            expression = block(expressionString);

            // В итоге каждая функция любой части должна делать так, чтобы expressionSymbol ссылался на символ оператора
            if (isExpressionOk) {
                if (expressionSymbol < expressionString.length())
                    while (expressionSymbol < expressionString.length()) {
                        char symbol = expressionString.charAt(expressionSymbol);
                        int prevExpressionSymbol = expressionSymbol;
                        expressionSymbol++;
                        double block = block(expressionString);
                        if (isExpressionOk) {
                            if (symbol == '-') {
                                expression -= block;
                            } else if (symbol == '+') {
                                expression += block;
                            } else {
                                isExpressionOk = false;
                                expressionSymbol = prevExpressionSymbol;
                                messages.add("Ожидалось: +, -, *, /, |, &. Возможно, введена переменная с неверным названием");
                                break;
                            }
                        } else
                            break;
                    }
            }

            if (isMinus)
                expression = -expression;

        }
        return expression;
    }

    private double block(String expressionString) {
        // �?тоговое значение

        double block = 0;

        block = part(expressionString);
        if (isExpressionOk) {
            if (expressionSymbol < expressionString.length())
                while (expressionSymbol < expressionString.length() && (expressionString.charAt(expressionSymbol) == '*' || expressionString.charAt(expressionSymbol) == '/')) {
                    char symbol = expressionString.charAt(expressionSymbol);
                    expressionSymbol++;
                    double part = part(expressionString);
                    if (isExpressionOk) {
                        if (symbol == '/') {
                            if(part == 0){
                                messages.add("Деление на ноль");
                                isExpressionOk = false;
                                return 0;
                            }

                            block = block / part;
                        }
                        if (symbol == '*') {
                            block = block * part;
                        }
                    } else
                        break;
                }
        }

        return block;
    }

    private double part(String expressionString) {
        double part = 0;

        part = littlePart(expressionString);

        if (isExpressionOk) {
            if (expressionSymbol < expressionString.length())
                while (expressionSymbol < expressionString.length() && (expressionString.charAt(expressionSymbol) == '|' || expressionString.charAt(expressionSymbol) == '&')) {
                    char symbol = expressionString.charAt(expressionSymbol);
                    expressionSymbol++;
                    double tinyPart = littlePart(expressionString);
                    if (isExpressionOk) {
                        if (symbol == '|') {
                            part = part + tinyPart;
                        }
                        if (symbol == '&') {
                            part = part * tinyPart;
                        }
                    } else
                        break;
                }
        }

        return part;
    }

    private double littlePart(String expressionString) {
        // Отсекаем лишнее. Лишнее здесь всё, что идёт до текущего символа
        String neededPartOfExpression = expressionString.substring(expressionSymbol);
        boolean minus = false;

        if (neededPartOfExpression.length() > 0) {


            Pattern floatOrNumberFirstInString = Pattern.compile("((\\(.*\\))(\\d+\\.\\d+)|(\\d+)).*");
            Pattern floatOrNumberFirstInStringQuad = Pattern.compile("((\\[.*\\])(\\d+\\.\\d+)|(\\d+)).*");
            Pattern bracketFirstInString = Pattern.compile("\\(.*\\).*");
            Pattern bracketFirstInStringQuad = Pattern.compile("\\[.*\\].*");
            Pattern floatOrNumber = Pattern.compile("(\\d+\\.\\d+)|(\\d+)");
            Pattern braket = Pattern.compile("(\\(.*\\))");
            Pattern braketQuad = Pattern.compile("(\\[.*\\])");
            Pattern mark = Pattern.compile("[а-яА-Я]\\d\\d?\\d?");
            Matcher m;

            // Для определения, что спереди - скобка или не скобка
            char firstLetter = neededPartOfExpression.charAt(0);

            if(firstLetter == '-'){
                messages.add("Такая конструкция запрещена по БНФ");
//                minus = true;
                expressionSymbol++;
//                neededPartOfExpression = neededPartOfExpression.substring(1);
                isExpressionOk = false;
            }

            firstLetter = neededPartOfExpression.charAt(0);

            if (firstLetter == '(') {
                // Впереди скобка
                m = bracketFirstInString.matcher(neededPartOfExpression);
                if (m.matches()) {
                    m = braket.matcher(neededPartOfExpression);
                    if (m.find()) {
                        int expressionSymb = expressionSymbol;
                        String expInBrackets = m.group(0).substring(1);
                        expInBrackets = expInBrackets.substring(0, expInBrackets.length() - 1);
                        double exp = expression(expInBrackets);

                        expressionSymbol = expressionSymb + expressionSymbol;

                        // Следующие операции связаны с тем, что нам нужно правильно указать на ошибку в скобке
                        if (isExpressionOk)
                            expressionSymbol += 2;
                        else
                            expressionSymbol++;

                        if(minus){
                            return -exp;
                        } else
                            return exp;
                    }
                } else {
                    messages.add("Незакрытая скобка");
                    isExpressionOk = false;
                }
            } else if (firstLetter == '[') {
                // Впереди скобка
                depth++;
                if(depth > 2){
                    messages.add("Максимальная вложенность квадратных скобок - 2");
                    isExpressionOk = false;
                    return 0;
                }
                m = bracketFirstInStringQuad.matcher(neededPartOfExpression);
                if (m.matches()) {
                    m = braketQuad.matcher(neededPartOfExpression);
                    if (m.find()) {
                        int expressionSymb = expressionSymbol;
                        String expInBrackets = m.group(0).substring(1);
                        expInBrackets = expInBrackets.substring(0, expInBrackets.length() - 1);
                        double exp = expression(expInBrackets);

                        expressionSymbol = expressionSymb + expressionSymbol;

                        // Следующие операции связаны с тем, что нам нужно правильно указать на ошибку в скобке
                        if (isExpressionOk)
                            expressionSymbol += 2;
                        else
                            expressionSymbol++;

                        if(minus){
                            return -exp;
                        } else
                            return exp;
                    }
                } else {
                    messages.add("Незакрытая скобка");
                    isExpressionOk = false;
                }
            } else {
                m = floatOrNumberFirstInString.matcher(neededPartOfExpression);
                if (m.matches()) {
                    // Впереди число

                    // Вытаскиваем это число
                    m = floatOrNumber.matcher(neededPartOfExpression);
                    if (m.find()) {
                        String numberString = m.group(0);
                        if(numberString.contains("8")||numberString.contains("9"))     {
                            messages.add("Только восьмеричная система счисления");
                            isExpressionOk = false;
                            if(numberString.contains("8")   )
                                expressionSymbol+=numberString.indexOf("8");
                            else
                                expressionSymbol+=numberString.indexOf("9");
                            return 0;
                        }
                        expressionSymbol += numberString.length();

                        double number = Double.valueOf(numberString);


                        if(minus){
                            return -number;
                        } else {
                            return number;
                        }
                    }
                } else {
                    // Впереди не понятно, что

                    m = mark.matcher(neededPartOfExpression);

                    if (m.find()) {
                        String variable = m.group(0);
                        expressionSymbol += variable.length();
                        if (variables.containsKey(variable)) {

                            if(minus){
                                return -variables.get(variable);
                            } else {
                                return variables.get(variable);
                            }
                        } else {
                            if(minus){
                                return -1;
                            } else{
                                return 1;
                            }
                        }
                    }


                    if (expressionSymbol != 0)
                        expressionSymbol--;

                    if(expressionString.charAt(expressionSymbol) == ')' || expressionString.charAt(expressionSymbol) == ']' ){
                        expressionSymbol = expressionString.length();
                        messages.add("Лишняя закрывающая скобка");
                        isExpressionOk = false;
                    } else {
                        if(words.get(i-1).equals("ввод")){
                            messages.add("Пропущена точка с запятой");
                            expressionSymbol--;
                            isExpressionOk = false;
                            return 0;
                        }

                        messages.add("Недопустимый символ или недопустимая конструкция в математическом выражении");
                        isExpressionOk = false;
                    }
                }
            }
        } else {
            isExpressionOk = false;

            if (expressionString.charAt(expressionSymbol - 1) == ']') {
                expressionSymbol--;
                messages.add("Лишняя скобка");
            } else {
                messages.add("Ожидалось число или открывающая скобка");
                // Для правильного выделения
                expressionSymbol--;
            }

        }

        return 0;
    }

    private boolean safeIncrement() {
        if (isDebug)
            messages.add("passed: " + words.get(i));

        i++;
        if (i >= words.size()) {
            messages.add("Неожиданный конец строки");
            // Для подчёркивания правильного слова (последнего)
            i--;
            return false;
        }
        return true;
    }

    private boolean safeIncrement(String message) {
        i++;
        if (i >= words.size()) {
            messages.add(message);
            // Для подчёркивания правильного слова (последнего)
            i--;
            return false;
        }
        return true;
    }


    public ArrayList<String> getMessages() {
        return messages;
    }

    boolean isExpressionOk() {
        return isExpressionOk;
    }

    int getExpressionSymbol() {
        return expressionSymbol;
    }

    String getBadExpression() {
        return badExpression;
    }

    int getSelectionStart() {
        return selectionStart;
    }

    int getSelectionEnd() {
        return selectionEnd;
    }

    int getBadExpressionBeginIndex() {
        return badExpressionBeginIndex;
    }

    int getBadExpressionEndIndex() {
        return badExpressionEndIndex;
    }
}

