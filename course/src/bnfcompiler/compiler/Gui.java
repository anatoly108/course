package bnfcompiler.compiler;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Gui {

    Interpretator interpretator = new Interpretator();
    JTextPane textArea;
    JTextArea messageArea;

    public Gui() {

        JFrame frame = new JFrame("Компилятор");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.X_AXIS));

        textArea = new JTextPane();
        Font font = new Font("sanserif", Font.PLAIN, 20);
        textArea.setFont(font);

        messageArea = new JTextArea(5, 15);
        messageArea.setLineWrap(true);
        font = new Font("sanserif", Font.PLAIN, 15);
        messageArea.setFont(font);
        messageArea.setEditable(false);
        messageArea.setText("Наберите код программы и нажмите кнопку 'Проверить'");

        JTextArea bnfArea = new JTextArea(5, 1);
        bnfArea.setLineWrap(true);
        font = new Font("sanserif", Font.PLAIN, 15);
        bnfArea.setFont(font);
        bnfArea.setEditable(false);
        bnfArea.setText("Язык = «программа» Звенья «конец»\n" +
                "Звенья = Звено ;… Звено\n" +
                "Звено = «Ввод» Слово … Слово\n" +
                "Слово = Метка «:» Переменная «=» Прав. часть\n" +
                "Прав. часть = </ «-» /> Блок Зн1… Блок\n" +
                "Зн1 = «+» ! «-»\n" +
                "Блок = Блок1 Зн2… Блок1\n" +
                "Зн2 = «*» ! «/»\n" +
                "Блок1 = Блок2 Зн3… Блок2\n" +
                "Зн3 = «&» ! «|»\n" +
                "Блок2 = «!» Блок3\n" +
                "Блок3 = Переменная ! Вещ. ! «(» Прав. часть «)» !  «[» Прав. часть «]» \n" +
                "Переменная = Буква </ Цифра />  </ Цифра /> </ Цифра />\n" +
                "Метка = Целое\n" +
                "Целое = Цифра … Цифра\n" +
                "Вещ. = Целое «.» Целое\n" +
                "Цифра = «0» ! «1» ! «2» ! … ! «7»\n" +
                "Буква = «А» ! «Б» ! «В» ! … ! «Я»\n");

        JScrollPane scrolledPane1 = new JScrollPane(textArea);
        scrolledPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrolledPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JScrollPane scrolledPane2 = new JScrollPane(messageArea);
        scrolledPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrolledPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrolledPane2.setMaximumSize(new Dimension(1000, 200));

        JScrollPane scrolledPane3 = new JScrollPane(bnfArea);
        scrolledPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrolledPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JButton button = new JButton("Проверить");
        font = new Font("sanserif", Font.BOLD, 15);
        button.setFont(font);
        button.addActionListener(new CompileListener());
        button.setAlignmentX(JComponent.LEFT_ALIGNMENT);

        upperPanel.add(scrolledPane1);
//        upperPanel.add(scrolledPane3);
        panel.add(upperPanel);
        panel.add(Box.createVerticalStrut(5));
        panel.add(button);
        panel.add(Box.createVerticalStrut(5));
        panel.add(scrolledPane2);

        frame.pack();
        frame.add(panel);
        frame.setSize(600, 600);
        frame.setVisible(true);
    }

    public class CompileListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String program = textArea.getText();

            AbstractDocument doc;
            StyledDocument styledDoc = textArea.getStyledDocument();
            doc = (AbstractDocument) styledDoc;

            SimpleAttributeSet redAttribute = new SimpleAttributeSet();
            StyleConstants.setForeground(redAttribute, Color.red);

            SimpleAttributeSet normalAttribute = new SimpleAttributeSet();
            StyleConstants.setForeground(normalAttribute, Color.black);

            ArrayList<String> words = new ArrayList<String>();

            ArrayList<String> splitedText = interpretator.split(program, ":", true);
            ArrayList<String> splitedTextTmp = new ArrayList<String>();
            for (String textToSplit : splitedText) {
                ArrayList<String> textToSplitSplited = interpretator.split(textToSplit, " ", false);
                splitedTextTmp.addAll(textToSplitSplited);
            }
            splitedText = splitedTextTmp;

            /**
             * Разбиение на слова кода программы
             */
            for (String word : splitedText) {

                if (!word.equals("")) {
                    if (word.contains("\n")) {
                        String lastWord = "";
                        String[] splited = word.split("[\\r | \\n]");
//                        String[] splited = word.split("[\\r* | \\n*]");
                        Pattern p = Pattern.compile("[\\r* | \\n*]+.+");
                        Matcher m = p.matcher(word);
                        p = Pattern.compile(".+[\\r* | \\n*]+");
                        Matcher m2 = p.matcher(word);
                        int i = 0;
                        boolean isInBegining = m.matches();
                        boolean isInEnd = m2.matches();
                        for (String subWord : splited) {
                            if (!subWord.equals("")) {
                                if (isInBegining) {
                                    words.add("\r\n" + subWord + " ");
                                } else if (isInEnd && i == splited.length - 1) {
                                    words.add(subWord + "\r\n");
                                } else {
                                    String wordToInsert = subWord + " ";
                                    if (i != splited.length - 1)
                                        wordToInsert = wordToInsert + "\r\n";
                                    words.add(wordToInsert);
                                }
                            }
                            i++;
                        }
                    } else {
                        word += " ";
                        words.add(word);
                    }
                }
            }

            if (!textArea.getText().equals("")) {
                if (interpretator.compile(textArea.getText())) {

                    textArea.setText("");
                    int i = 0;
                    for (String word : words) {
                        try {
                            doc.insertString(doc.getLength(), word/* + " "*/, normalAttribute);
                        } catch (BadLocationException e1) {
                            e1.printStackTrace();
                        }
                        i++;
                    }

                    ArrayList<String> messages = interpretator.getMessages();

                    messageArea.setText("");

                    // Выдаём сообщения
                    for (String message : messages) {
                        messageArea.append(message + "\n");
                    }

                    messageArea.append("Проверка пройдена успешно");
                } else {
                    ArrayList<String> messages = interpretator.getMessages();

                    messageArea.setText("");

                    // Выдаём сообщения
                    for (String message : messages) {
                        messageArea.append(message + "\n");
                    }

                    textArea.setText("");

                    // Цикл вывода слов зависит от типа ошибки: в мат. выражении или в просто в программе
                    if (interpretator.isExpressionOk()) {
                        // Возможно, нужно: if (interpretator.hasErrors()) ...
                        int i = 0;
                        for (String word : words) {
                            if (i == interpretator.getIterator()) {
                                try {
                                    doc.insertString(doc.getLength(), word, redAttribute);
                                    // Чтобы сделать дальнейший ввод снова чёрным
                                    doc.insertString(doc.getLength(), "", normalAttribute);
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                try {
                                    doc.insertString(doc.getLength(), word + "", normalAttribute);
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            i++;
                        }
                    } else {
                        for (int i = 0; i < words.size(); i++) {
                            String word = words.get(i);
                            if (i == interpretator.getBadExpressionBeginIndex()) {
                                // Попали в начальный индекс мат. выражения с ошибкой
                                // Теперь будем постепенно выводить слова мат. выражения, пока не поймём, что подошли к expressionSymbol (т.е., к ошибке)
                                try {
                                    String expression = "";
                                    boolean found = false;
                                    for (i = i; i <= interpretator.getBadExpressionEndIndex(); i++) {
                                        word = words.get(i);
                                        String prevExpression = expression;
                                        expression += words.get(i);
                                        expression = expression.trim();
                                        if (found || expression.length() < interpretator.getExpressionSymbol() + 1) {
                                            // Тут мы либо уже нашли слово выражения с ошибкой, либо ещё не дошли до него
                                            doc.insertString(doc.getLength(), String.valueOf(words.get(i)), normalAttribute);
                                        } else {
                                            // Это означает, что в word содержится ошибка
                                            // Нужно подсчитать все символы и понять, какой подчеркнуть
                                            int errorIndex = 0;
                                            if (i == interpretator.getBadExpressionBeginIndex()) {
                                                errorIndex = interpretator.getExpressionSymbol();
                                            } else {
                                                errorIndex = interpretator.getExpressionSymbol() - prevExpression.length();
                                            }
                                            for (int k = 0; k < word.length(); k++) {
                                                char letter = word.charAt(k);
                                                if (k == errorIndex) {
                                                    // Выяснили, что тут ошибка
                                                    doc.insertString(doc.getLength(), String.valueOf(letter), redAttribute);
                                                } else {
                                                    doc.insertString(doc.getLength(), String.valueOf(letter), normalAttribute);
                                                }
                                            }
                                            found = true;
                                        }
                                    }
                                    // Нужно сделать корректное значение i
                                    i--;
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                try {
                                    doc.insertString(doc.getLength(), word + "", normalAttribute);
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                }
            } else {
                messageArea.setText("Наберите код программы и нажмите кнопку 'Скомпилировать'");
            }
        }
    }


}