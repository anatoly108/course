package bnfcompiler.compiler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CompilerOld {
    // Сюда будем добавлять сообщения (как консоль)
    private ArrayList<String> messages = new ArrayList<String>();
    // Все слова
    private ArrayList<String> words = new ArrayList<String>();

    // Переменные для мат. выражения
    // Нет ли ошибки в выражении
    private boolean isExpressionOk = true;
    // Индекс текущего символа в выражении
    private int expressionSymbol = 0;
    // Выражение с ошибкой, если таковое есть
    private String badExpression = "";
    // Автомат Мура или Мили
    private boolean isMoore;

    // Начальный и конечный индексы "плохого" математического выражения
    private int badExpressionBeginIndex = 0;
    private int badExpressionEndIndex = 0;

    // Индекс начала выделения красным
    private int selectionStart = 0;
    // Индекс конца выделения красным
    private int selectionEnd = 0;


    private int i = 0; // Наш итератор (инкрементим функцией)

    // В режиме отладки выводятся дополнительные сообщения: пройденные слова; строки, полученные методами
    private boolean isDebug = false;

    public CompilerOld() {

    }

    /**
     * Разбиение текста регулярным выражением с возможностью сохранения этого выражения
     *
     * @param text
     * @param regex
     * @param leaveRegex
     * @return ArrayList с разделённым текстом
     */
    public ArrayList<String> split(String text, String regex, boolean leaveRegex) {
        ArrayList<String> result = new ArrayList<String>();
        String[] splitRes = text.split(regex);
        for (int j = 0; j < splitRes.length; j++) {
            if (!splitRes[j].equals(""))
                result.add(splitRes[j]);
            if (leaveRegex) {
                // Добавляем regex везде, кроме конца
                if (j < splitRes.length - 1)
                    result.add(regex);
            }
        }
        return result;
    }

    /**
     * Этот метод нужен для определения слова, которое нужно выделить красным
     *
     * @return Индекс слова в words
     */
    public int getIterator() {
        return i;
    }

    /**
     * Функция компиляции
     * Манипулирует блоками текста между объектами элементов и выдаёт конечный результат компиляции true/false
     *
     * @param programText Текст программы
     * @return true/false Скомпилировалось/не скомпилировалось
     */
    public boolean compile(String programText) {
        words.clear();
        messages.clear();
        i = 0;
        isMoore = false;

        // Убираем управляющие символы
        programText = programText.replaceAll("\\r|\\n", " ");

        /**
         * Разбиваем на отдельные слова
         */

        ArrayList<String> splitedText = this.split(programText, ":", true);
        ArrayList<String> splitedTextTmp = new ArrayList<String>();
        for (String textToSplit : splitedText) {
            ArrayList<String> textToSplitSplited = this.split(textToSplit, ",", true);
            splitedTextTmp.addAll(textToSplitSplited);
        }
        splitedText = splitedTextTmp;
        splitedTextTmp = new ArrayList<String>();
        for (String textToSplit : splitedText) {
            ArrayList<String> textToSplitSplited = this.split(textToSplit, ";", true);
            splitedTextTmp.addAll(textToSplitSplited);
        }
        splitedText = splitedTextTmp;
        splitedTextTmp = new ArrayList<String>();
        for (String textToSplit : splitedText) {
            ArrayList<String> textToSplitSplited = this.split(textToSplit, " ", false);
            splitedTextTmp.addAll(textToSplitSplited);
        }
        splitedText = splitedTextTmp;
        words = splitedText;

        if (words.size() > 0) {

            if (!words.get(i).equals("fsmachine")) {
                messages.add("Отсутствует 'fsmachine' в начале");
                return false;
            }

            // Инкрементим
            if (!safeIncrement("Отсутствует имя (слово) после fsmachine"))
                return false;

            if (!name(words.get(i))) {
                messages.add("Неверное имя (слово) после fsmachine");
                return false;
            }

            if (!safeIncrement("После имени должно быть 'states'"))
                return false;

            if (!words.get(i).equals("states")) {
                messages.add("После имени должно быть 'states'");
                return false;
            }


            if (!safeIncrement("После 'states' должно быть 'Moore' или 'Mealy'"))
                return false;


            if (words.get(i).equals("Moore")) {
                // Moore
                if (!safeIncrement("После 'Moore' должны идти описания состояний автомата"))
                    return false;
                isMoore = true;
            } else if (words.get(i).equals("Mealy")) {
                // Mealy
                if (!safeIncrement("После 'Mealy' должны идти описания состояний автомата"))
                    return false;
            } else {
                messages.add("После 'states' должно быть 'Moore' или 'Mealy'");
                return false;
            }

            do {

                if (!state(isMoore)) {
                    return false;
                }

                if (i <= words.size() - 1) {
                    if (!words.get(i).equals("reserved")) {
                        if (words.get(i).equals(";")) {
                            if (!safeIncrement("Неожиданный конец строки. Возможно, лишняя точка с запятой"))
                                return false;
                        } else {
                            // Если встречаем лишний выходной сигнал в автомате Мили
                            if (!isMoore && signalOut(false)) {
                                // Для правильного выделения
                                i--;
                                messages.add("Лишний выходной сигнал: при задании автомата Мили вершине не присваивается выходной сигнал");
                                return false;
                            }
                            messages.add("Распознано как начало нового состояния. Состояния разделяются точкой с запятой");
                            return false;
                        }
                    }
                } else
                    break;

            } while (!words.get(i).equals("reserved") && i <= words.size() - 1);

            return true;
        } else
            return false;

    }

    private boolean state(boolean isMoore) {

        if (words.get(i).equals("begin"))
            if (!safeIncrement())
                return false;
        if (words.get(i).equals("end")) {
            if (!safeIncrement())
                return false;
            if (words.get(i).equals("begin")) {
                messages.add("begin должен идти перед end");
                // Для правильного выделения
                i--;
                return false;
            }
        }

        if (vertice(words.get(i))) {
            if (isMoore)
                if (!safeIncrement("При задании автомата Мура после имени вершины должен идти выходной сигнал этой вершины"))
                    return false;
        } else {
            messages.add("Неверное имя для вершины: " + words.get(i));
            return false;
        }

        if (isMoore) {
            if (!signalOut(true)) {
                messages.add("Ошибка в выходном сигнале. Выходной сигнал может быть строкой или математическим выражением");
                return false;
            }
        } else
            i++;

        if (i <= words.size() - 1) {
            if (words.get(i).equals(":")) {
                do {
                    if (!safeIncrement("Неожиданный конец строки: ожидалось перечесление последующих вершин"))
                        return false;

                    if (isMoore) {
                        if (!nextMoore())
                            return false;
                    } else {
                        if (!nextMealy())
                            return false;
                    }

                    // Если не конец цикла, то должна быть запятая
                    if (i <= words.size() - 1) {
                        if (!words.get(i).equals(";")) {
                            if (!words.get(i).equals(",")) {
                                messages.add("Ожидалась последующая вершина или другое состояние. Последующие вершины разделяются запятыми, состояния - точкой с запятой, возможно, пропущен один из этих знаков препинания");
                                return false;
                            } else {
                                if (i == words.size() - 1) {
                                    messages.add("После запятой должна идти последующая вершина");
                                }
                            }
                        } else {
                            if (words.get(i).equals(",")) {
                                messages.add("Лишняя запятая");
                                return false;
                            }
                        }
                    } else {
                        break;
                    }

                } while (!words.get(i).equals(";") && i <= words.size() - 1);
            }
        }

        return true;
    }

    private boolean nextMoore() {
        if (vertice(words.get(i))) {
            if (!safeIncrement("После имени последующей вершины должен идти входной сигнал к этой вершине"))
                return false;

            if (!signalIn()) {
                messages.add("Ошибка во входном сигнале: входным сигналом может быть строка, начинающаяся и заканчивающаяся двойными кавычками, или EPS");
                return false;
            }
        } else {
            messages.add("Неверное имя для вершины");
            return false;
        }

        return true;
    }

    private boolean nextMealy() {
        if (vertice(words.get(i))) {
            if (!safeIncrement("После имени последующей вершины должен идти входной сигнал к этой вершине"))
                return false;

            if (!signalIn()) {
                // Поправка для правильного выделения
                messages.add("Ошибка во входном сигнале: входным сигналом может быть строка, начинающаяся и заканчивающаяся двойными кавычками, или EPS");
                return false;
            }

            if (i <= words.size() - 1) {
                if (!signalOut(true)) {
                    return false;
                }
            } else {
                messages.add("При задании автомата Мили после входного сигнала должен идти выходной (строка или математическое выражение)");
                return false;
            }
        } else {
            messages.add("Ожидалось имя вершины. Неверное имя для вершины");
            return false;
        }
        return true;
    }

    private boolean signalIn() {
        if (words.get(i).equals("EPS")) {
            i++;

            return true;
        } else
            return string();
    }

    private boolean signalOut(boolean allowMessages) {

        if (string()) {
            return true;
        } else {
            String expression = "";
            boolean error = false;
            String errorMsg = "";
            badExpressionBeginIndex = i;
            while (i < words.size() && !words.get(i).equals(",") && !words.get(i).equals(":") && !words.get(i).equals(";")) {
                // Если вершина, то, значит, пропущено двоеточие или запятая
                char symbol = words.get(i).charAt(0);
                char lastSymbol = 0;
                if (!expression.equals(""))
                    lastSymbol = expression.charAt(expression.length() - 1);
                if (!vertice(words.get(i))) {
                    boolean isSpaceAllowed = true;
                    if (lastSymbol != 0)
                        isSpaceAllowed = (symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/' || symbol == '^' || symbol == '(' || symbol == ')') || (lastSymbol == '+' || lastSymbol == '-' || lastSymbol == '*' || lastSymbol == '/' || lastSymbol == '^' || lastSymbol == '(' || lastSymbol == ')');
                    if (expression.equals("") || isSpaceAllowed) {
                        expression += words.get(i);
                    } else {
                        if (allowMessages) {
                            errorMsg = "Лишний пробел в математическом выражении";
                        }
                        error = true;
                        break;
                    }

                } else {
                    if (allowMessages) {
                        if (isMoore)
                            errorMsg = "Пропущены двоеточие или точка с запятой после математического выражения";
                        else
                            errorMsg = "Пропущена запятая после математического выражения";
                    }
                    error = true;
                    break;
                }

                i++;
            }
            if (error) {
                messages.add(errorMsg);
                return false;
            }

            badExpressionEndIndex = i - 1;

            double result = expression(expression);
            if (isExpressionOk)
                messages.add(expression + " = " + String.valueOf(result));
            else {
                badExpression = expression;
                // Указываем на красным на expression
                i--;
                return false;
            }

            return true;
        }
    }

    /**
     * Подсчёт правой части
     *
     * @param expressionString строка, которую нужно подсчитать. Оставить "" (пустую строку), если нет конкретной правой части
     * @return число
     */
    private double expression(String expressionString) {
        double expression = 0;
        expressionSymbol = 0;
        isExpressionOk = true;
        // Есть ли минус
        boolean isMinus = false;

        if (expressionString.length() > 0) {
            if (expressionString.charAt(expressionSymbol) == '-') {
                expressionSymbol++;
                isMinus = true;
            }

            expression = block(expressionString);

            // В итоге каждая функция любой части должна делать так, чтобы expressionSymbol ссылался на символ оператора
            if (isExpressionOk) {
                if (expressionSymbol < expressionString.length())
                    while (expressionSymbol < expressionString.length()) {
                        char symbol = expressionString.charAt(expressionSymbol);
                        int prevExpressionSymbol = expressionSymbol;
                        expressionSymbol++;
                        double block = block(expressionString);
                        if (isExpressionOk) {
                            if (symbol == '-') {
                                expression -= block;
                            } else if (symbol == '+') {
                                expression += block;
                            } else {
                                isExpressionOk = false;
                                expressionSymbol = prevExpressionSymbol;
                                messages.add("Ожидалось: +, -, *, /, ^");
                                break;
                            }
                        } else
                            break;
                    }
            }

            if (isMinus)
                expression = -expression;

        }
        return expression;
    }

    private double block(String expressionString) {
        // Итоговое значение

        double block = 0;

        block = part(expressionString);
        if (isExpressionOk) {
            if (expressionSymbol < expressionString.length())
                while (expressionSymbol < expressionString.length() && (expressionString.charAt(expressionSymbol) == '*' || expressionString.charAt(expressionSymbol) == '/')) {
                    char symbol = expressionString.charAt(expressionSymbol);
                    expressionSymbol++;
                    double part = part(expressionString);
                    if (isExpressionOk) {
                        if (symbol == '/') {
                            block = block / part;
                        }
                        if (symbol == '*') {
                            block = block * part;
                        }
                    } else
                        break;
                }
        }

        return block;
    }

    private double part(String expressionString) {
        double part = 0;

        part = littlePart(expressionString);

        if (isExpressionOk) {
            if (expressionSymbol < expressionString.length())
                while (expressionSymbol < expressionString.length() && expressionString.charAt(expressionSymbol) == '^') {
                    expressionSymbol++;
                    double littlePart = littlePart(expressionString);
                    if (isExpressionOk) {
                        part = Math.pow(part, littlePart);
                    }
                }
        }

        return part;
    }

    private double littlePart(String expressionString) {
        // Отсекаем лишнее. Лишнее здесь всё, что идёт до текущего символа
        String neededPartOfExpression = expressionString.substring(expressionSymbol);

        if (neededPartOfExpression.length() > 0) {

            Pattern floatOrNumberFirstInString = Pattern.compile("((\\(.*\\))(\\d+\\.\\d+)|(\\d+)).*");
            Pattern bracketFirstInString = Pattern.compile("\\(.*\\).*");
            Pattern floatOrNumber = Pattern.compile("(\\d+\\.\\d+)|(\\d+)");
            Pattern braket = Pattern.compile("(\\(.*\\))");
            Matcher m;

            // Для определения, что спереди - скобка или не скобка
            char firstLetter = neededPartOfExpression.charAt(0);

            if (firstLetter == '(') {
                // Впереди скобка
                m = bracketFirstInString.matcher(neededPartOfExpression);
                if (m.matches()) {
                    m = braket.matcher(neededPartOfExpression);
                    if (m.find()) {
                        int expressionSymb = expressionSymbol;
                        String expInBrackets = m.group(0).substring(1);
                        expInBrackets = expInBrackets.substring(0, expInBrackets.length() - 1);
                        double exp = expression(expInBrackets);

                        expressionSymbol = expressionSymb + expressionSymbol;

                        // Следующие операции связаны с тем, что нам нужно правильно указать на ошибку в скобке
                        if (isExpressionOk)
                            expressionSymbol += 2;
                        else
                            expressionSymbol++;

                        return exp;
                    }
                } else {
                    messages.add("Незакрытая скобка");
                    isExpressionOk = false;
                }
            } else {
                m = floatOrNumberFirstInString.matcher(neededPartOfExpression);
                if (m.matches()) {
                    // Впереди число

                    // Вытаскиваем это число
                    m = floatOrNumber.matcher(neededPartOfExpression);
                    if (m.find()) {
                        String numberString = m.group(0);
                        expressionSymbol += numberString.length();
                        double number = Double.valueOf(numberString);

                        return number;
                    }
                } else {
                    // Впереди не понятно, что
                    if (expressionSymbol != 0)
                        expressionSymbol--;
                    messages.add("Недопустимый символ или недопустимая конструкция в математическом выражении");
                    isExpressionOk = false;
                }
            }
        } else {
            isExpressionOk = false;

            if (expressionString.charAt(expressionSymbol - 1) == ')') {
                expressionSymbol--;
                messages.add("Лишняя скобка");
            } else {
                messages.add("Ожидалось число или открывающая скобка");
                // Для правильного выделения
                expressionSymbol--;
            }

        }

        return 0;
    }

    private boolean string() {
        String string = "";
        for (int j = i; j < words.size(); j++) {
            string += words.get(j) + " ";
        }
        Pattern p = Pattern.compile("^\"[\\w+\\s]+\"");
        Matcher m = p.matcher(string);

        if (m.find()) {
            String str = m.group(0);
            if (isDebug)
                messages.add("got string: '" + str + "'");
            for (String word : str.split(" ")) {
                if (!word.equals("")) {
                    i++;
                }
            }
            return true;
        }
        return false;
    }

    private boolean name(String word) {
        Pattern p = Pattern.compile("\"\\w+\"");
        Matcher m = p.matcher(word);
        if (m.matches())
            return true;
        else return false;
    }

    private boolean vertice(String word) {
        if (isDebug)
            messages.add("got vertice: '" + word + "'");
        Pattern p = Pattern.compile("[a-zA-Z]\\w+");
        Matcher m = p.matcher(word);
        if (m.matches())
            return true;
        else return false;
    }

    private boolean safeIncrement() {
        if (isDebug)
            messages.add("passed: " + words.get(i));

        i++;
        if (i >= words.size()) {
            messages.add("Неожиданный конец строки");
            // Для подчёркивания правильного слова (последнего)
            i--;
            return false;
        }
        return true;
    }

    private boolean safeIncrement(String message) {
        i++;
        if (i >= words.size()) {
            messages.add(message);
            // Для подчёркивания правильного слова (последнего)
            i--;
            return false;
        }
        return true;
    }


    public ArrayList<String> getMessages() {
        return messages;
    }

    boolean isExpressionOk() {
        return isExpressionOk;
    }

    int getExpressionSymbol() {
        return expressionSymbol;
    }

    String getBadExpression() {
        return badExpression;
    }

    int getSelectionStart() {
        return selectionStart;
    }

    int getSelectionEnd() {
        return selectionEnd;
    }

    int getBadExpressionBeginIndex() {
        return badExpressionBeginIndex;
    }

    int getBadExpressionEndIndex() {
        return badExpressionEndIndex;
    }
}

